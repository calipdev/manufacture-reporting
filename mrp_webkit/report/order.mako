<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style type="text/css">
        ${css}

            body, table {
                font-family: helvetica;
                font-size: 9px;
            }

            .header {
                margin-left: 0px;
                text-align: left;
                font-size: 10px;
            }

            .title {
                font-size: 14px;
                font-weight: bold;
            }

            .list_table {
                text-align: center;
                border-collapse: collapse;
            }

            .list_table td {
                text-align: left;
                font-size: 10px;
            }

            .list_table th {
                font-size: 10px;
            }

            .list_table thead {
                display: table-header-group;
            }

            .address table {
                font-size: 9px;
                border-collapse: collapse;
                margin: 0px;
                padding: 0px;
            }

            .address .invoice {
                margin-top: 10px;
            }

            .address .recipient {
                margin-right: 120px;
                float: right;
            }

            table .address_title {
                font-weight: bold;
            }

            .address td.name {
                font-weight: bold;
            }

            td.amount, th.amount {
                text-align: right;
                white-space: nowrap;
            }

            td.desc, th.desc {
                text-align: left;
            }

            h1 {
                font-size: 13px;
                font-weight: bold;
            }

            tr.line .note {
                font-size: 8px;
            }

            tr.line {
                margin-bottom: 10px;
            }
            /*
             * Mi Css
            */

            .act_as_table {
                display: table;
                width: 100%;
                text-align:center;
                table-layout: fixed
            }

            .act_as_row  {
                display: table-row;
            }

            .act_as_cell {
                display: table-cell;
                text-align:left;
                font-size:10px;
                padding-right:3px;
                padding-left:3px;
                padding-top:5px;
                padding-bottom:3px;
                clear:both;
            }

            .act_table,
            .act_as_cell,
            .act_as_row {
                word-wrap: break-word;
            }

            .contenedor {
                color: red;
                font-size:100px;
                padding-top:350px;
                -webkit-transform: rotate(-45deg);
            }

            .contenedor_principal {
                position: relative;
                width: 100%;
            }

            .contenedor_contenido {
                position: relative;
                z-index: 2;
            }

            .contenedor_cancelado {
                position: absolute;
                top:0px;
                left:-60px;
                z-index: 1;
            }
        </style>

    </head>
    <body>
        %for o in objects:
            <div class="act_as_table" style="padding-top:10px;">
                <div class="act_as_row">
                    <div class="act_as_cell" style="border:1px solid black; width:20%;">
                        <div style="position: absolute; top: 22px; bottom: 0px;">
                                ${helper.embed_logo_by_name('logo',130)|n}
                        </div>
                    </div>
                    <div class="act_as_cell" style="border:1px solid black; width:60%">
                        <div style="text-align:center; font-size:18px;">
                            <br>
                            <b>RUTA DE MANUFACTURA</b>
                        </div>
                    </div>
                    <div class="act_as_cell" style="border:1px solid black;">
                        <div class="act_as_table" >
                            <div class="act_as_row">
                                <div class="act_as_cell" style="border-bottom:1px solid black; color:black; text-align:right;">
                                    FR-08-03
                                </div>
                            </div>
                            <div class="act_as_row">
                                <div class="act_as_cell" style="text-align:right; color:black;">
                                    Edicion 8
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <table class="title" width="100%">
                <tr>
                    <td width="100%">
                        <h3><b>${ _('Núm. orden producción') }: ${o.name}</b></h3>
                    </td>
                </tr>
            </table>


            <table width="100%" class="list_table2">
                <tr>
                    <td width="35%">
                        <b>${ _('Source Document') }</b>
                    </td>
                    <td width="40%">
                        <b>${ _('Product') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('Quantity') }</b>
                    </td>
                </tr>
            </table>
            <table width="100%" class="list_table2">
                <tr>
                    <td width="35%">
                        ${o.origin or ''}
                    </td>
                    <td width="40%">
                        ${o.product_id and o.product_id.code or ''} ${o.product_id and o.product_id.name or ''}
                    </td>
                    <td width="25%">
                        ${formatLang(o.product_qty)} ${o.product_id and o.product_uom and o.product_uom.name or ''}
                    </td>
                </tr>
            </table>
            <br/>

            <table width="100%" class="list_table2">
                <tr>
                    <td width="25%">
                        <b>${ _('Proceso Productivo') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('Número Dibujo') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('Ruta Producción') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('Fecha programada') }</b>
                    </td>
                </tr>
            </table>
            <table width="100%" class="list_table2">
                <tr>
                    <td width="25%">
                        ${o.routing_id.name or ''}
                    </td>
                    <td width="25%">
                        ${o.num_dibujo or ''}
                    </td>
                    <td width="25%">
                        ${o.ruta_prod or ''}
                    </td>
                    <td width="25%">
                        ${formatLang(o.date_planned, date_time = True)}
                    </td>
                </tr>
            </table>
            <br/>

            <table width="100%" class="list_table2">
                <tr>
                    <td width="25%">
                        <b>${ _('Fecha de Inicio') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('Printing date') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('Ref. empresa') }</b>
                    </td>
                    <td width="25%">
                        <b>${ _('SO Number') }</b>
                    </td>
                </tr>
            </table>
            <table width="100%" class="list_table2">
                <tr>
                     <td width="25%">
                        ${formatLang(o.date_planned, date_time = True)}
                    </td>
                    <td width="25%">
                        ${formatLang(time.strftime('%Y-%m-%d'),date = True)}
                    </td>
                    <td width="25%">
                        ${'sale_ref' in o._columns.keys() and o.sale_ref or ''}
                    </td>
                    <td width="25%">
                        ${'sale_name' in o._columns.keys() and o.sale_name or ''}
                    </td>
                </tr>
            </table>
            <br/>

            %if o.workcenter_lines ==[]:

            %else:
                <table  width="100%">
                    <tr>
                        <td style="text-align:left;" width="100%">
                             <h4><b>${ _('Work Orders') }</b></h4>
                        </td>
                    </tr>
                </table>
                <table width="100%" class="list_table2">
                    <tr>
                        <td width="10%">
                            <b>${ _('Sequence') }</b>
                        </td>
                        <td width="30%">
                            <b>${ _('Name') }</b>
                        </td>
                        <td width="20%">
                            <b>${ _('WorkCenter') }</b>
                        </td>
                        <td width="5%">
                            <b>${ _('No. Of Cycles') }</b>
                        </td>
                        <td width="10%">
                            <b>${ _('No. Of Hours') }</b>
                        </td>
                        <td width="10%">
                            <b>${ _('Fecha Inicio') }</b>
                        </td>
                        <td width="10%">
                            <b>${ _('Fecha Fin') }</b>
                        </td>
                        <td width="10%">
                            <b>${ _('Qty') }</b>
                        </td>
                    </tr>
                </table>
                %for line2 in o.workcenter_lines:
                    <table width="100%" class="list_table1">
                        <tr>
                            <td width="10%">
                                ${str(line2.sequence)}
                            </td>
                            <td width="30%">
                                ${line2.name}
                            </td>
                            <td width="20%">
                                ${line2.workcenter_id and line2.workcenter_id.name or ''}
                            </td>
                            <td width="5%">
                                ${formatLang(line2.cycle)}
                            </td>
                            <td width="10%">
                                ${formatLang(line2.hour)}
                            </td>
                            <td width="10%">
                                ${formatLang(line2.date_start, date_time = True)}
                            </td>
                            <td width="10%">
                                ${formatLang(line2.date_finished, date_time = True)}
                            </td>
                            <td width="10%">
                                ${line2.qty}
                            </td>
                        </tr>
                    </table><br/>
                %endfor
            %endif

            <table class="title" width="100%">
                   <tr>
                       <td width="100%">
                            <h3>${ _('Bill Of Material') }</h3>
                       </td>
                   </tr>
           </table>
           <table style="border-bottom:2px solid black;font-family: Helvetica; font-size:11px;" width="100%" >
                <tr>
                    <td style="text-align:left; " width="55%">
                        <b>${ _('Product') }</b>
                    </td>
                    <td style="text-align:right;" width="10%">
                        <b>${ _('Quantity') }</b>
                    </td>
                    <td style="text-align:center;" width="20%">
                        <b>${ _('Source Location') }</b>
                    </td>
                    <td style="text-align:center;" width="15%">
                        <b>${ _('Destination Location') }</b>
                    </td>
                </tr>
            </table>

             %if o.move_lines ==[]:

             %else:
                <table   style="font-family: Helvetica; font-size:11px;" width="100%">
                   <tr>
                       <td  width="100%">
                            <b>${ _('Products to Consume') }</b>
                       </td>
                   </tr>
                </table>
                %for line in o.move_lines:
                    <table width="100%" class="list_table">
                        <tr>
                            <td style="text-align:left;" width="55%">
                                ${line.product_id and line.product_id.code or ''} ${line.product_id and line.product_id.name or ''}
                            </td>
                            <td style="text-align:right;" width="10%">
                                ${formatLang( line.product_qty)} ${line.product_uom and line.product_uom.name or ''}
                            </td>
                            <td style="text-align:center;"width="20%">
                                ${line.location_id and line.location_id.name or ''}
                            </td>
                            <td style="text-align:center;"width="15%">
                                ${line.location_dest_id and line.location_dest_id.name or ''}
                            </td>
                        </tr>
                    </table>
                %endfor
             %endif

             %if o.move_lines2 ==[]:

             %else:
                 <table class="title" width="100%">
                    <tr>
                        <td width="100%">
                            <h5><b>${ _('Consumed Products') }</b></h5>
                       </td>
                   </tr>
                 </table>
                 %for line2 in o.move_lines2:
                    <table width="100%" class="list_table1">
                        <tr>
                            <td style="text-align:left;"width="55%">
                                ${line2.product_id and line2.product_id.code or ''} ${line2.product_id and line2.product_id.name or ''}
                            </td>
                            <td style="text-align:right;"width="10%">
                                ${formatLang( line2.product_qty)} ${line2.product_uom and line2.product_uom.name or ''}
                            </td>
                            <td style="text-align:center;"width="20%">
                                ${line2.location_id and line2.location_id.name or ''}
                            </td>
                            <td style="text-align:center;"width="15%">
                                ${line2.location_dest_id and line2.location_dest_id.name or ''}
                            </td>
                        </tr>
                    </table>
                %endfor
             %endif
             <p style="page-break-after:always"></p>
        %endfor
    </body>
</html>
